/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.tamago.run;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.JPanel;


/**
 *
 * @author Student
 */
public class Move extends JPanel  {
    int x = 1;
    int modulo;
    int mood = 100;
    int hunger = 100;
    int health = 100;
    int level = 1;
    int score = 0;
    int q=1;
    int opsi, opsi1, opsi2, opsi3, opsi4;
    String nama = "";

    

    boolean buttonPress = false;
    boolean inGame = false;
    boolean inQuiz = false;
    boolean mati = false;
    boolean first = true;
    

    private Image panah;
    private Image food;
    private Image happy;
    private Image healthy;
    private Image quiz;
    private Image choices;
    private Image bener;
    private Image salah;
    private Image player;
    private Image playersakit;
    private Image dead;
    private Image backgroundgame;
    private Image backgroundgrave;
    private Image backgroundquiz;
    private Image splash;
    private Image pertanyaan;
    private Image nes;
    private Image genesis;


    
    public void loadImages() {
        ImageIcon tunjuk = new ImageIcon("src/resources/panah.png");
        ImageIcon makanan = new ImageIcon("src/resources/food.png");
        ImageIcon seneng = new ImageIcon("src/resources/happy.png");
        ImageIcon sehat = new ImageIcon("src/resources/health.png");
        ImageIcon kuis = new ImageIcon("src/resources/quiz.png");
        ImageIcon pilih = new ImageIcon("src/resources/option.png");
        ImageIcon tru = new ImageIcon("src/resources/benar.png");
        ImageIcon fals = new ImageIcon("src/resources/salah.png");
        ImageIcon sples = new ImageIcon("src/resources/splash.png");
        ImageIcon pemain = new ImageIcon("src/resources/kuchipatchi.png"); //tracing karakter kuchipatchi dari tamagochi
        ImageIcon pemainsakit = new ImageIcon("src/resources/kuchipatchisick.png"); //tracing karakter kuchipatchi dari tamagochi
        ImageIcon mati = new ImageIcon("src/resources/grave.png");
        ImageIcon pert = new ImageIcon("src/resources/pertanyaan.png");
        ImageIcon bekgron = new ImageIcon("src/resources/background.png"); //source http://www.misucell.com/group/pixel-wallpaper/
        ImageIcon bekgronkubur = new ImageIcon("src/resources/backgroundgrave.png");// source https://imgur.com/r/gaming/fra5ll4
        ImageIcon bekgronquiz = new ImageIcon("src/resources/backgroundquiz.png");// source https://imgur.com/GVpTmgq
        ImageIcon ness = new ImageIcon("src/resources/nes.png");
        ImageIcon genesnes = new ImageIcon("src/resources/genesis.png");
        
        splash = sples.getImage();
        backgroundquiz =bekgronquiz.getImage();
        backgroundgrave = bekgronkubur.getImage();
        backgroundgame = bekgron.getImage();
        panah = tunjuk.getImage();
        food = makanan.getImage();
        happy = seneng.getImage();
        healthy = sehat.getImage();
        quiz = kuis.getImage();
        choices = pilih.getImage();
        bener = tru.getImage();
        salah = fals.getImage();
        player = pemain.getImage();
        playersakit = pemainsakit.getImage();
        dead = mati.getImage();
        pertanyaan = pert.getImage();
        nes = ness.getImage();
        genesis = genesnes.getImage();
        


    }
    public void StartGame(){
        repaint();
        gamestatus();
        loadImages();
    }
    
    public Move(){
        addKeyListener(new KeyListener(){
            @Override
            public void keyTyped(KeyEvent e){
               
            } 
            @Override
            public void keyReleased(KeyEvent e){

            }
            @Override
            public void keyPressed(KeyEvent e){
                  int key = e.getKeyCode();
                  if(level<3){
                     if(x>1){
                        if (key == KeyEvent.VK_A) {
                            x = x - 1;
                        }
                    }
                    if(x<4){
                        if (key == KeyEvent.VK_D) {
                            x = x + 1;
                        }
                    }
                    
                    if (key == KeyEvent.VK_S) {
                        buttonPress = true;
                        action();
                        x = 1;
                    } 
                  }else{
                    if(x>1){
                        if (key == KeyEvent.VK_A) {
                            x = x - 1;
                        }
                    }
                    if(x<3){
                        if (key == KeyEvent.VK_D) {
                            x = x + 1;
                        }
                    }
                    
                    if (key == KeyEvent.VK_S) {
                        buttonPress = true;
                        action();
                        x = 1;
                    }  
                  }
                    
            }
        });
             setFocusable(true);   
    }
    
    //Draw Line, bot, player     
    
    
    
    public void gamestatus(){
        if(inGame == true){
            mood = mood - 1;
            hunger = hunger - 1;
            health = health - 1;
            if(mood>100){
                modulo = mood%100;
                mood = mood - modulo;
            }if(hunger>100){
                modulo = hunger%100;
                hunger = hunger - modulo;
            }if(health>100){
                modulo = health%100;
                health = health - modulo;
            }
            
            
        }else if(inQuiz == true){
            if(q == 6){
                if(score == 5){
                    level++;
                    score = 0;
                    q = 1;
                    inGame = true;
                    inQuiz = false;
                    opsi = 0;
                    opsi1 = 0;
                    opsi2 = 0;
                    opsi3 = 0;
                    opsi4 = 0;
                }else{
                    inGame = true;
                    inQuiz = false;
                    score = 0;
                    q = 1;
                    opsi = 0;
                    opsi1 = 0;
                    opsi2 = 0;
                    opsi3 = 0;
                    opsi4 = 0;
                }
            }   
        }
        if(mood < 1 || health < 1|| hunger < 1){
            inGame = false;
            mati = true;
        }
    }
    
    public void action(){
        
        if(first == true){
            if(buttonPress == true){
                inGame = true;
                first = false;
            }
        }
        
        if(inGame == true){
            if(buttonPress == true){
              if(x == 1){
                  hunger = hunger + 5;
                  mood = mood - 3;
                  health = health - 2;
              }else if(x == 2){
                  hunger = hunger - 3;
                  mood = mood - 2;
                  health = health + 5;
              }else if(x == 3){
                  hunger = hunger -2;
                  mood = mood + 5;
                  health = health - 3;
              }else if(x == 4){
                  inGame = false;
                  inQuiz = true;
              }
              buttonPress = false;
            }  
        }
        if(inQuiz == true){
            if(level == 1){
                if(buttonPress == true){
                    if(q == 1){
                        if(x==1){
                            score++;
                            q++;
                            opsi = 1;
                        }else{
                            q++;
                            opsi = 2;
                        }
                    }else if(q == 2){
                        if(x==3){
                            score++;
                            q++;
                            opsi1 = 1;
                        }else{
                            q++;
                            opsi1 = 2;
                        }
                    }else if(q == 3){
                        if(x==2){
                            score++;
                            q++;
                            opsi2 = 1;
                        }else{
                            q++;
                            opsi2 = 2;
                        }
                    }else if(q == 4){
                        if(x==2){
                            score++;
                            q++;
                            opsi3 = 1;
                        }else{
                            q++;
                            opsi3 = 2;
                        }
                    }else if(q == 5){
                        if(x==4){
                            score++;
                            q++;
                            opsi4 = 1;
                        }else{
                            q++;
                            opsi4 = 2;
                        }
                        
                    }
                    buttonPress = false;
                } 
            }
            if(level == 2){
                if(buttonPress == true){
                    if(q == 1){
                        if(x==2){
                            score++;
                            q++;
                            opsi = 1;
                        }else{
                            q++;
                            opsi = 2;
                        }
                    }else if(q == 2){
                        if(x==4){
                            score++;
                            q++;
                            opsi1 = 1;
                        }else{
                            q++;
                            opsi1 = 2;
                        }
                    }else if(q == 3){
                        if(x==1){
                            score++;
                            q++;
                            opsi2 = 1;
                        }else{
                            q++;
                            opsi2 = 2;
                        }
                    }else if(q == 4){
                        if(x==4){
                            score++;
                            q++;
                            opsi3 = 1;
                        }else{
                            q++;
                            opsi3 = 2;
                        }
                    }else if(q == 5){
                        if(x==3){
                            score++;
                            q++;
                            opsi4 = 1;
                        }else{
                            q++;
                            opsi4 = 2;
                        }
                        
                    }
                    buttonPress = false;
                } 
            }
        }
    }
    
    
    public void paint(Graphics g){
        super.paintComponent(g);
        Graphics2D g2d = (Graphics2D)g;
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,RenderingHints.VALUE_ANTIALIAS_ON);
        g2d.setColor(Color.black);
        g2d.drawImage(splash, 400,0, this);
        g2d.fillRect(400, 0, 10, 400);
        Font small = new Font("Helvetica", Font.BOLD, 12);
        g.setFont(small);
        if(first == true){
            Font big = new Font("Helvetica", Font.BOLD, 20);
            g.setFont(big);
            g.drawString("Controls: ", 15, 15);
            g.drawString("- A & D - Scroll Menu", 15, 50);
            g.drawString("- S - Pilih Opsi", 15, 85);
            g.drawString("Tekan S Untuk Lanjut Ke Game ", 15, 150);
        }
        if(mati == true){
            g2d.drawImage(backgroundgrave, 0, 0, this);
            g2d.setColor(Color.red);
            g.drawString(nama,180, 250);
            g2d.drawImage(dead, 150, 250, this);
            gameover(g);
        }
        if(inGame == true){
            g2d.drawImage(backgroundgame, 0, 0, this);
            g.drawString(nama, 180, 250);
            g2d.drawImage(food, 45, 45, this);
            g2d.drawImage(healthy, 135, 45, this);
            g2d.drawImage(happy, 225, 45, this);
            if(mood>30 || health > 30|| hunger >30){
                g2d.drawImage(player, 150, 250, this);
            }
            if(mood < 31 || health < 31|| hunger < 31){
                g2d.drawImage(playersakit, 150, 250, this);
            }
            
            if(level == 2){
                g2d.drawImage(nes, 50, 250, this);
            }
            if(level == 3){
                g2d.drawImage(nes, 50, 250, this);
                g2d.drawImage(genesis, 250, 250, this);
            }
            
            if(level<3){
                g2d.drawImage(quiz, 315, 45, this);
            }
            g.drawString("Mood: " + mood, 420, 45);
            g.drawString("Health: " + health, 420, 30);
            g.drawString("Hunger: " + hunger, 420, 15);
            g.drawString("Level: " + level, 420, 60);

            if(x==1){
                g2d.drawImage(panah, 50, 15, this);
            }
            if(x==2){
                g2d.drawImage(panah, 140, 15, this);
            }
            if(x==3){
                g2d.drawImage(panah, 230, 15, this);
            }
            if(x == 4){
                g2d.drawImage(panah, 320, 15, this);
            }
        }
        if(inQuiz == true){
            g2d.drawImage(backgroundquiz, 0, 0, this);
            g2d.drawImage(pertanyaan, 0, 0, this);
            g2d.setColor(Color.black);
            g.drawString("Quiz Time!!", 15, 30);
            g.drawString("Score: " + score, 420, 15);
            g.drawString("Level: " + level, 420, 30);
            g.drawString("Pertanyaan 1: ", 420, 45);
            g.drawString("Pertanyaan 2: ", 420, 110);
            g.drawString("Pertanyaan 3: ", 420, 175);
            g.drawString("Pertanyaan 4: ", 420, 240);
            g.drawString("Pertanyaan 5: ", 420, 305);
            g2d.drawImage(choices, 10, 108, this);
            g2d.drawImage(choices, 190, 108, this);
            g2d.drawImage(choices, 10, 208, this);
            g2d.drawImage(choices, 190, 208, this);
            
            if(opsi == 1){
                g2d.drawImage(bener, 420, 50, this);
                
            }if(opsi == 2){
                g2d.drawImage(salah, 420, 50, this);
                
            }if(opsi1 == 1){
                g2d.drawImage(bener, 420, 115, this);
                
            }if(opsi1 == 2){
                g2d.drawImage(salah, 420, 115, this);
                
            }
            if(opsi2 == 1){
                g2d.drawImage(bener, 420, 180, this);
                
            }if(opsi2 == 2){
                g2d.drawImage(salah, 420, 180, this);
                
            }
            if(opsi3 == 1){
                g2d.drawImage(bener, 420, 245, this);
                
            }if(opsi3 == 2){
                g2d.drawImage(salah, 420, 245, this);
                
            }
            if(opsi4 == 1){
                g2d.drawImage(bener, 420, 310, this);
                
            }if(opsi4 == 2){
                g2d.drawImage(salah, 420, 310, this);
                
            }
            
            if(x==1){
                g2d.drawImage(panah, 50, 90, this);
            }
            if(x==2){
                g2d.drawImage(panah, 240, 90, this);
            }
            if(x==3){
                g2d.drawImage(panah, 50, 190, this);
            }
            if(x == 4){
                g2d.drawImage(panah, 240, 190, this);
            }
            
            if(level == 1){
                if(q == 1){
                    g.drawString("1. Perusahaan manakah yang merilis konsol NES?", 15, 50);                    
                    g.drawString("A. Nintendo", 15, 125);                    
                    g.drawString("B. Sega", 195, 125);                    
                    g.drawString("C. Atari", 15, 225);                    
                    g.drawString("D. Neo Geo", 195, 225);
                }else if(q == 2){
                    g.drawString("2. Apa nama konsol 8-bit yang dibuat oleh Sega?", 15, 50);                    
                    g.drawString("A. Mega Drive", 15, 125);                    
                    g.drawString("B. 32x", 195, 125);                    
                    g.drawString("C. Master System", 15, 225);                    
                    g.drawString("D. Dreamcast", 195, 225);
                }else if(q == 3){
                    g.drawString("3. Apa nama game berbasis lcd yang dirilis oleh Nintendo?", 15, 50);                    
                    g.drawString("A. Virtual Boy", 15, 125);                    
                    g.drawString("B. Game & Watch", 195, 125);                    
                    g.drawString("C. Wrist Game", 15, 225);                    
                    g.drawString("D. The R-Zone", 195, 225);
                }else if(q == 4){
                    g.drawString("4. Apa konsol terakhir dari Atari?", 15, 50);                    
                    g.drawString("A. Lynx", 15, 125);                    
                    g.drawString("B. Jaguar", 195, 125);                    
                    g.drawString("C. Game.com", 15, 225);                    
                    g.drawString("D. Channel F", 195, 225);
                }else if(q == 5){
                    g.drawString("5. Siapa maskot konsol Playstation 1?", 15, 50);                    
                    g.drawString("A. Sonic", 15, 125);
                    g.drawString("B. Gex", 195, 125);
                    g.drawString("C. Mario", 15, 225);
                    g.drawString("D. Crash", 195, 225);
                }
            }
            if(level == 2){
                if(q == 1){
                    g.drawString("1. Tengen adalah anak perusahaan dari... ", 15, 50);                    
                    g.drawString("A. Sega", 15, 125);                    
                    g.drawString("B. Atari", 195, 125);                    
                    g.drawString("C. Tiger", 15, 225);                    
                    g.drawString("D. Microsoft", 195, 225);
                }else if(q == 2){
                    g.drawString("2. Konsol apa yang pertama kali menggunakan cartridge?", 15, 50);                    
                    g.drawString("A. NES", 15, 125);                    
                    g.drawString("B. R-Zone", 195, 125);                    
                    g.drawString("C. Atari 2600", 15, 225);                    
                    g.drawString("D. Channel F", 195, 225);
                }else if(q == 3){
                    g.drawString("3. Famicom memiliki add on agar bisa bermain game dengan... ", 15, 50);                    
                    g.drawString("A. Disket", 15, 125);                    
                    g.drawString("B. CD", 195, 125);                    
                    g.drawString("C. Tape", 15, 220);                    
                    g.drawString("D. HDD", 195, 225);
                }else if(q == 4){
                    g.drawString("4. Apa judul game arcade pertama?", 15, 50);                    
                    g.drawString("A. Space Invader", 15, 125);                    
                    g.drawString("B. Galaga", 195, 125);                    
                    g.drawString("C. Pong", 15, 225);                    
                    g.drawString("D. Computer Space", 195, 225);
                }else if(q == 5){
                    g.drawString("5. Sebelum menjadi console standalone, Playstation merupakan... ", 15, 50);                    
                    g.drawString("A. DVD Player", 15, 125);
                    g.drawString("B. Add on Genesis", 195, 125);
                    g.drawString("C. Add on SNES", 15, 225);
                    g.drawString("D. Add on CDI", 195, 225);
                }
            }
        }
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }
    public void gameover(Graphics g){
        
        String msg = "Game Over";
        Font small = new Font("Helvetica", Font.BOLD, 50);
        FontMetrics metr = getFontMetrics(small);
        
        g.setColor(Color.red);
        g.setFont(small);
        g.drawString(msg, 75, 200);
    }
}
