/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.tamago.run;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 *
 * @author lenovo
 */
public class Start {
    public static void main (String[] args)throws InterruptedException{
        JFrame frame = new JFrame("Tamagotchi");
        Move game = new Move();
        String name = JOptionPane.showInputDialog(frame, "Siapa namamu?");
        game.setNama(name);
        frame.add(game);
        frame.setSize(600,400);
        frame.setResizable(false);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        while(true){
            game.StartGame();
            Thread.sleep(1000);
        }
    }
}
